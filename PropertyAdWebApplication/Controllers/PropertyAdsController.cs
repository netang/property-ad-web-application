﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PropertyAdWebApplication.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PropertyAdWebApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PropertyAdsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PropertyAdsController(ApplicationDbContext context)
        {
            _context = context;

            if (_context.PropertyAds.Count() == 0)
            {
                // Create a new Property Ad if collection is empty,
                // which means you can't delete all Property Ads.
                _context.PropertyAds.Add(new PropertyAd { Price = 400000.0, RoomsCount = 1 });
                _context.SaveChanges();
            }
        }

        // GET: api/PropertyAds
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<PropertyAd>>> GetPropertyAds()
        {
            return await _context.PropertyAds.ToListAsync();
        }

        // GET: api/PropertyAds/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<PropertyAd>> GetPropertyAd(long id)
        {
            var announcementItem = await _context.PropertyAds.FindAsync(id);

            if (announcementItem == null)
            {
                return NotFound();
            }

            return announcementItem;
        }

        // POST: api/PropertyAds
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<PropertyAd>> PostPropertyAd(PropertyAd item)
        {
            _context.PropertyAds.Add(item);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetPropertyAd), new { id = item.Id }, item);
        }

        // PUT: api/PropertyAds/5
        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> PutPropertyAd(long id, PropertyAd item)
        {
            if (id != item.Id)
            {
                return BadRequest();
            }

            _context.Entry(item).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        // DELETE api/Announcement/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
