﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PropertyAdWebApplication.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PropertyAdWebApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdPhoneNumberController : Controller
    {
        private readonly ApplicationDbContext _context;

        public AdPhoneNumberController(ApplicationDbContext context)
        {
            _context = context;
        }

        // PUT: api/AdPhoneNumber/5
        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> PutAdPhoneNumber(long id, PhoneNumber item)
        {
            if (id != item.Id)
            {
                return BadRequest();
            }

            _context.Entry(item).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
