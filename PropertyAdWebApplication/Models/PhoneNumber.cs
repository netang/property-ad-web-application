﻿namespace PropertyAdWebApplication.Models
{
    public class PhoneNumber
    {
        public long Id { get; set; }
        public string Number { get; set; }

        public long AnnouncementId { get; set; }
        public PropertyAd PropertyAd { get; set; }
    }
}
