﻿namespace PropertyAdWebApplication.Models
{
    /// <summary>
    /// Property Ads
    /// </summary>
    public class PropertyAd
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        /// <summary>
        /// Number of rooms
        /// </summary>
        public int RoomsCount { get; set; }
        /// <summary>
        /// Price of real estate
        /// </summary>
        public double Price { get; set; }
        /// <summary>
        /// Type of announcement. 1 is buy, 2 is sell
        /// </summary>
        public int Type { get; set; }
    }
}
