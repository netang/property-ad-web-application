﻿using Microsoft.EntityFrameworkCore;

namespace PropertyAdWebApplication.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<PropertyAd> PropertyAds { get; set; }
        public DbSet<PhoneNumber> AnnouncementTelephoneNumbers { get; set; }
    }
}
